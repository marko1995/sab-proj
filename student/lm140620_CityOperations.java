package student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import operations.CityOperations;

public class lm140620_CityOperations implements CityOperations {

	Connection connection = DB.getInstance().getConnection();
	
	@Override
	public int createCity(String name) {
		String insertQuery = "insert into cities values(?)";
		try (PreparedStatement ps = connection.prepareStatement(insertQuery,
						PreparedStatement.RETURN_GENERATED_KEYS);) {
			ps.setString(1, name);
			int i = ps.executeUpdate();
			try (ResultSet rs = ps.getGeneratedKeys();) {
				if (rs.next()) {
					return rs.getInt(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return i;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public List<Integer> getCities() {
		List<Integer> list = null;
		String sql = "select * from cities;";
		try (Statement statement = connection.createStatement(); ResultSet rs = statement.executeQuery(sql)) {
			list = new ArrayList<>();
			while (rs.next()) {
				list.add(rs.getInt("id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public int connectCities(int cityId1, int cityId2, int distance) {
		// TODO: Napisati trigger za proveru postojanja vise od jedne linije ???
		boolean exist = true;
		String sql = "select * from connections where (city1id=? and city2id=?) or (city1id=? and city2id=?);";

		if(cityId1 == cityId2) return -1;
		
		try (PreparedStatement ps = connection.prepareStatement(sql);) {
			ps.setInt(1, cityId1);
			ps.setInt(2, cityId2);
			ps.setInt(3, cityId2);
			ps.setInt(4, cityId1);
			try (ResultSet rs = ps.executeQuery();) {
				if (!rs.next()) {
					exist = false;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (!exist) {
			sql = "insert into connections values(?,?,?)";
			try (PreparedStatement ps = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);) {

				ps.setInt(1, cityId1);
				ps.setInt(2, cityId2);
				ps.setInt(3, distance);
				int i = ps.executeUpdate();
				try (ResultSet rs = ps.getGeneratedKeys();) {
					if (rs.next()) {
						return rs.getInt(1);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return i;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return -1;
	}

	@Override
	public List<Integer> getConnectedCities(int cityId) {
		String sql = "select * from connections where city1id=? or city2id=?;";
		List<Integer> list = null;
		try (PreparedStatement ps = connection.prepareStatement(sql);) {
			ps.setInt(1, cityId);
			ps.setInt(2, cityId);
			try (ResultSet rs = ps.executeQuery();) {
				list = new ArrayList<>();
				while (rs.next()) {
					int id1 = rs.getInt("city1id");
					int id2 = rs.getInt("city2id");
					int id = id1 != cityId ? id1 : id2;
					if(!list.contains(id)) {
						list.add(id);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Integer> getShops(int cityId) {
		String sql = "select * from shops where cityid=?;";
		List<Integer> list = new ArrayList<>();
		try (PreparedStatement ps = connection.prepareStatement(sql);) {
			ps.setInt(1, cityId);
			try (ResultSet rs = ps.executeQuery();) {
				while (rs.next()) {
					int id = rs.getInt("id");
					list.add(id);
				}
				return list;
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
