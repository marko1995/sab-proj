package student;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import operations.TransactionOperations;

public class lm140620_TransactionOperations implements TransactionOperations {

	Connection connection = DB.getInstance().getConnection();

	@Override
	public BigDecimal getBuyerTransactionsAmmount(int buyerId) {

		String sqlTransaction = "select priceWithSystemDiscount from user_transactions where userid=?;";
		try (PreparedStatement ps = connection.prepareStatement(sqlTransaction)) {
			ps.setInt(1, buyerId);
			try (ResultSet rs = ps.executeQuery()) {
				BigDecimal ammount = new BigDecimal("0").setScale(3);
				while (rs.next()) {
					BigDecimal bd = rs.getBigDecimal(1);
					ammount = ammount.add(bd.setScale(3));
				}
				return ammount;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new BigDecimal("-1").setScale(3);
	}

	@Override
	public BigDecimal getShopTransactionsAmmount(int shopId) {

		String sqlTransaction = "select price from shop_transactions where shopid=? and status='paid';";
		try (PreparedStatement ps = connection.prepareStatement(sqlTransaction)) {
			ps.setInt(1, shopId);
			try (ResultSet rs = ps.executeQuery()) {
				BigDecimal ammount = new BigDecimal("0").setScale(3);
				while (rs.next()) {
					ammount = ammount.add(rs.getBigDecimal(1).setScale(3));
				}
				return ammount;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new BigDecimal("-1").setScale(3);
	}

	@Override
	public List<Integer> getTransationsForBuyer(int buyerId) {
		String sqlTransaction = "select id from user_transactions where userid=? order by createdAt desc;";
		try (PreparedStatement ps = connection.prepareStatement(sqlTransaction)) {
			ps.setInt(1, buyerId);
			try (ResultSet rs = ps.executeQuery()) {
				List<Integer> list = new ArrayList<>();
				while (rs.next()) {
					list.add(rs.getInt(1));
				}
				return list;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int getTransactionForBuyersOrder(int orderId) {
		String sql = "select id from user_transactions where orderid=?;";

		try (PreparedStatement ps = connection.prepareStatement(sql)) {
			ps.setInt(1, orderId);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next())
					return rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public int getTransactionForShopAndOrder(int orderId, int shopId) {
		String sql = "select id from shop_transactions where orderid=? and shopid=?;";

		try (PreparedStatement ps = connection.prepareStatement(sql)) {
			ps.setInt(1, orderId);
			ps.setInt(2, shopId);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next())
					return rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public List<Integer> getTransationsForShop(int shopId) {
		String sqlTransaction = "select id from shop_transactions where shopid=? and finishedAt != null order by createdAt desc;";
		try (PreparedStatement ps = connection.prepareStatement(sqlTransaction)) {
			ps.setInt(1, shopId);
			try (ResultSet rs = ps.executeQuery()) {
				List<Integer> list = new ArrayList<>();
				while (rs.next()) {
					list.add(rs.getInt(1));
				}

				if (list.isEmpty())
					return null;
				return list;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Calendar getTimeOfExecution(int transactionId) {
		String sql = "select ut.createdAt, st.createdAt, ut.finishedAt, st.finishedAt from " + "transactions t "
				+ "left join user_transactions ut on t.id=ut.id " + "left join shop_transactions st on t.id=st.id "
				+ "where t.id=?;";
		try (PreparedStatement ps = connection.prepareStatement(sql)) {
			ps.setInt(1, transactionId);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					Date date11 = rs.getDate(1);
					Date date12 = rs.getDate(3);
					Date date1 = date12 != null ? date12 : date11;
					Date date21 = rs.getDate(2);
					Date date22 = rs.getDate(4);
					Date date2 = date22 != null ? date22 : date21;
					
					if (date1 != null) {
						Calendar calendar = Calendar.getInstance();
						calendar.clear();
						calendar.setTime(date1);
						return calendar;
					} else if (date2 != null) {
						Calendar calendar = Calendar.getInstance();
						calendar.clear();
						calendar.setTime(date2);
						return calendar;
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public BigDecimal getAmmountThatBuyerPayedForOrder(int orderId) {
		String sql = "select priceWithSystemDiscount from user_transactions where orderid=?;";
		try (PreparedStatement ps = connection.prepareStatement(sql)) {
			ps.setInt(1, orderId);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next())
					return rs.getBigDecimal(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public BigDecimal getAmmountThatShopRecievedForOrder(int shopId, int orderId) {
		String sql = "select price from shop_transactions where orderid=? and shopid=?;";
		try (PreparedStatement ps = connection.prepareStatement(sql)) {
			ps.setInt(1, orderId);
			ps.setInt(1, shopId);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next())
					return rs.getBigDecimal(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public BigDecimal getTransactionAmount(int transactionId) {
		String sql = "select ut.priceWithSystemDiscount, st.price from " + "transactions t "
				+ "left join user_transactions ut on t.id=ut.id "
				+ "left join shop_transactions st on t.id=st.id and st.status='paid' " + "where t.id=?;";
		try (PreparedStatement ps = connection.prepareStatement(sql)) {
			ps.setInt(1, transactionId);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					BigDecimal price1 = rs.getBigDecimal(1);
					BigDecimal price2 = rs.getBigDecimal(2);
					if (price1 != null) {
						return price1.setScale(3);
					} else if (price2 != null) {
						return price2.setScale(3);
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public BigDecimal getSystemProfit() {
		BigDecimal bigDecimal = new BigDecimal("0").setScale(3);
		String sqlProfit = "select sum(allPrice - price) from shop_transactions where status='paid';";

		try (Statement s = connection.createStatement()) {
			try (ResultSet rs = s.executeQuery(sqlProfit);) {
				if (rs.next()) {
					BigDecimal bd = rs.getBigDecimal(1);
					if (bd != null && bd.doubleValue() != 0.0)
						bigDecimal = bd.setScale(3);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return bigDecimal;
	}

}
