package student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import operations.ShopOperations;

public class lm140620_ShopOperations implements ShopOperations {

	Connection connection = DB.getInstance().getConnection();
	
	@Override
	public int createShop(String name, String cityName) {
		int id = getCityId(connection, cityName);

		if (id != -1) {
			String insertQuery = "insert into shops values(?, ?, ?, 0)";
			try (PreparedStatement ps = connection.prepareStatement(insertQuery,
							PreparedStatement.RETURN_GENERATED_KEYS);) {
				ps.setString(1, name);
				ps.setInt(2, id);
				ps.setInt(3, 0);
				int i = ps.executeUpdate();
				try (ResultSet rs = ps.getGeneratedKeys();) {
					if (rs.next()) {
						return rs.getInt(1);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return i;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return -1;
	}

	@Override
	public int setCity(int shopId, String cityName) {
		int id = getCityId(connection, cityName);

		if (id != -1) {
			String insertQuery = "update shops set cityid=? where id=?";
			try (PreparedStatement ps = connection.prepareStatement(insertQuery)) {
				ps.setInt(1, id);
				ps.setInt(2, shopId);
				return ps.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return -1;
	}

	@Override
	public int getCity(int shopId) {
		String query = "select CityId from shops where id=?";
		try (PreparedStatement ps = connection.prepareStatement(query);) {
			ps.setInt(1, shopId);
			try (ResultSet rs = ps.executeQuery();) {
				if (rs.next()) {
					return rs.getInt(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return -1;
	}

	@Override
	public int setDiscount(int shopId, int discountPercentage) {
		String insertQuery = "update shops set discount=? where id=?";
		try (PreparedStatement ps = connection.prepareStatement(insertQuery)) {
			ps.setInt(1, discountPercentage);
			ps.setInt(2, shopId);
			return ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public int increaseArticleCount(int articleId, int increment) {
		String sql = "update articles set quantity=quantity+? where id=?";
		try (PreparedStatement ps = connection.prepareStatement(sql);) {
			ps.setInt(1, increment);
			ps.setInt(2, articleId);
			return ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public int getArticleCount(int articleId) {
		String sql = "select quantity from articles where id=?";
		try (PreparedStatement ps = connection.prepareStatement(sql);) {
			ps.setInt(1, articleId);
			try (ResultSet rs = ps.executeQuery();) {
				if (rs.next()) {
					return rs.getInt(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public List<Integer> getArticles(int shopId) {
		String sql = "select id from articles where shopid=?";
		List<Integer> list = null;
		try (PreparedStatement ps = connection.prepareStatement(sql);) {
			ps.setInt(1, shopId);
			try (ResultSet rs = ps.executeQuery();) {
				list = new ArrayList<>();
				while (rs.next()) {
					int id = rs.getInt(1);
					list.add(id);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public int getDiscount(int shopId) {
		String sql = "select discount from shops where id=?";
		try (PreparedStatement ps = connection.prepareStatement(sql);) {
			ps.setInt(1, shopId);
			try (ResultSet rs = ps.executeQuery();) {
				if (rs.next()) {
					return rs.getInt(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	private int getCityId(Connection connection, String cityName) {
		int id = -1;
		String query = "select * from cities where name=?";

		try (PreparedStatement ps = connection.prepareStatement(query);) {
			ps.setString(1, cityName);
			try (ResultSet rs = ps.executeQuery();) {
				if (rs.next()) {
					id = rs.getInt(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}
}
