package student;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

import operations.GeneralOperations;

public class lm140620_GeneralOperations implements GeneralOperations {

	Connection connection = DB.getInstance().getConnection();

	@Override
	public void setInitialTime(Calendar time) {
		Calendar calendar = time;
		String sql = "insert into date values(?);";
		try (PreparedStatement ps = connection.prepareStatement(sql)) {
			Date date = new Date(time.getTimeInMillis());
			ps.setDate(1, date);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Calendar time(int days) {
		String sql = "update date set date=?;";
		Calendar calendar = getCurrentTime();
		if (calendar != null && days > 0) {
			try (PreparedStatement ps = connection.prepareStatement(sql)) {
				calendar.add(Calendar.DAY_OF_MONTH, days);
				Date date = new Date(calendar.getTimeInMillis());
				ps.setDate(1, date);
				ps.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}

			String sqlOders = "select id, days_to_shop, days_to_user from orders where status='sent'";
			String sqlUpdateOrder = "update orders set status=?, receivetime=?, days_to_shop = ?, days_to_user = ? where id=?;";
			try (Statement ps = connection.createStatement()) {
				try (ResultSet rs = ps.executeQuery(sqlOders)) {
					while (rs.next()) {
						int orderId = rs.getInt(1);
						int daysToShop = rs.getInt(2);
						int daysToUser = rs.getInt(3);

						int moreDays = 0;

						for (int i = 0; i < days; i++) {
							if (daysToShop > 0)
								daysToShop--;
							else if (daysToUser > 0)
								daysToUser--;
							else
								moreDays--;
						}

						String status = "sent";
						Date receiveDate = null;
						if (daysToShop == 0 && daysToUser == 0) {
							status = "arrived";
							Calendar receiveCalendar = (Calendar) calendar.clone();
							receiveCalendar.add(Calendar.DAY_OF_MONTH, moreDays);
							receiveDate = new Date(receiveCalendar.getTimeInMillis());
						}

						try (PreparedStatement ps1 = connection.prepareStatement(sqlUpdateOrder)) {
							ps1.setString(1, status);
							ps1.setDate(2, receiveDate);
							ps1.setInt(3, daysToShop);
							ps1.setInt(4, daysToUser);
							ps1.setInt(5, orderId);

							ps1.executeUpdate();

						} catch (SQLException e) {
							e.printStackTrace();
						}

					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return calendar;
	}

	@Override
	public Calendar getCurrentTime() {
		String sql = "select date from date";
		Calendar calendar = null;
		try (PreparedStatement ps = connection.prepareStatement(sql)) {
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					Date date = rs.getDate(1);
					calendar = Calendar.getInstance();
					calendar.setTime(date);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return calendar;
	}

	@Override
	public void eraseAll() {
		String sql = "delete from date; delete from transports; delete from user_transactions; delete from shop_transactions; "
				+ "delete from transactions; delete from ordered_articles; delete from orders; delete from users;"
				+ "delete from articles; delete from shops; delete from connections; delete from cities;";
		try (Statement statement = connection.createStatement();) {
			boolean i = statement.execute(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
