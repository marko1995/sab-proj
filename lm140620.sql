create database Prodaja
go

use Prodaja
go 


CREATE TABLE [articles]
( 
	[id]                 integer  NOT NULL  IDENTITY ,
	[name]               varchar(100)  NULL ,
	[price]              integer  NULL ,
	[quantity]           integer  NULL ,
	[ShopId]             integer  NULL 
)
go

ALTER TABLE [articles]
	ADD PRIMARY KEY  CLUSTERED ([id] ASC)
go

CREATE TABLE [cities]
( 
	[id]                 integer  NOT NULL  IDENTITY ,
	[name]               varchar(100)  NULL 
)
go

ALTER TABLE [cities]
	ADD PRIMARY KEY  CLUSTERED ([id] ASC)
go

ALTER TABLE [cities]
	ADD UNIQUE ([name]  ASC)
go

CREATE TABLE [connections]
( 
	[id]                 integer  NOT NULL  IDENTITY ,
	[City1Id]            integer  NULL ,
	[City2Id]            integer  NULL ,
	[distance]           integer  NULL 
)
go

ALTER TABLE [connections]
	ADD PRIMARY KEY  CLUSTERED ([id] ASC)
go

CREATE TABLE [date]
( 
	[id]                 integer  NOT NULL  IDENTITY ,
	[date]               datetime  NULL 
)
go

ALTER TABLE [date]
	ADD PRIMARY KEY  CLUSTERED ([id] ASC)
go

CREATE TABLE [ordered_articles]
( 
	[ArticleId]          integer  NOT NULL ,
	[OrderId]            integer  NOT NULL ,
	[quantity]           integer  NULL 
)
go

ALTER TABLE [ordered_articles]
	ADD PRIMARY KEY  CLUSTERED ([OrderId] ASC,[ArticleId] ASC)
go

CREATE TABLE [orders]
( 
	[id]                 integer  NOT NULL  IDENTITY ,
	[status]             varchar(100)  NOT NULL 
		 DEFAULT  'created'
		CHECK  ( [status]='created' OR [status]='sent' OR [status]='arrived' ),
	[UserId]             integer  NOT NULL ,
	[senttime]           datetime  NULL ,
	[receivetime]        datetime  NULL ,
	[days_to_user]       integer  NULL ,
	[days_to_shop]       integer  NULL ,
	[shopId]             integer  NULL 
)
go

ALTER TABLE [orders]
	ADD PRIMARY KEY  CLUSTERED ([id] ASC)
go

CREATE TABLE [shop_transactions]
( 
	[id]                 integer  NOT NULL ,
	[orderId]            integer  NULL ,
	[shopId]             integer  NULL ,
	[price]              decimal(10,3)  NULL ,
	[status]             char(18)  NOT NULL 
		 DEFAULT  'pending'
		CHECK  ( [status]='pending' OR [status]='paid' ),
	[createdAt]          datetime  NOT NULL ,
	[finishedAt]         datetime  NULL ,
	[allPrice]           decimal(10,3)  NULL 
)
go

ALTER TABLE [shop_transactions]
	ADD PRIMARY KEY  CLUSTERED ([id] ASC)
go

CREATE TABLE [shops]
( 
	[id]                 integer  NOT NULL  IDENTITY ,
	[name]               varchar(100)  NULL ,
	[CityId]             integer  NULL ,
	[discount]           integer  NULL ,
	[credit]             decimal(10,3)  NULL 
		 DEFAULT  0
)
go

ALTER TABLE [shops]
	ADD PRIMARY KEY  CLUSTERED ([id] ASC)
go

ALTER TABLE [shops]
	ADD UNIQUE ([name]  ASC)
go

CREATE TABLE [transactions]
( 
	[id]                 integer  NOT NULL  IDENTITY 
)
go

ALTER TABLE [transactions]
	ADD PRIMARY KEY  CLUSTERED ([id] ASC)
go

CREATE TABLE [transports]
( 
	[id]                 integer  NOT NULL  IDENTITY ,
	[days]               integer  NULL ,
	[count]              integer  NULL ,
	[orderId]            integer  NULL ,
	[cityFromId]         integer  NULL ,
	[cityToId]           integer  NULL 
)
go

ALTER TABLE [transports]
	ADD PRIMARY KEY  CLUSTERED ([id] ASC)
go

CREATE TABLE [user_transactions]
( 
	[id]                 integer  NOT NULL ,
	[hasSystemDiscount]  bit  NULL ,
	[price]              decimal(10,3)  NULL ,
	[priceWithDiscount]  decimal(10,3)  NULL ,
	[priceWithSystemDiscount] decimal(10,3)  NULL ,
	[status]             char(18)  NULL 
		CHECK  ( [status]='pending' OR [status]='paid' ),
	[orderId]            integer  NULL ,
	[userId]             integer  NULL ,
	[createdAt]          datetime  NULL ,
	[finishedAt]         datetime  NULL 
)
go

ALTER TABLE [user_transactions]
	ADD PRIMARY KEY  CLUSTERED ([id] ASC)
go

CREATE TABLE [users]
( 
	[id]                 integer  NOT NULL  IDENTITY ,
	[name]               varchar(100)  NULL ,
	[CityId]             integer  NULL ,
	[credit]             decimal(10,3)  NULL 
)
go

ALTER TABLE [users]
	ADD PRIMARY KEY  CLUSTERED ([id] ASC)
go


ALTER TABLE [articles]
	ADD  FOREIGN KEY ([ShopId]) REFERENCES [shops]([id])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [connections]
	ADD  FOREIGN KEY ([City1Id]) REFERENCES [cities]([id])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [connections]
	ADD  FOREIGN KEY ([City2Id]) REFERENCES [cities]([id])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [ordered_articles]
	ADD  FOREIGN KEY ([ArticleId]) REFERENCES [articles]([id])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [ordered_articles]
	ADD  FOREIGN KEY ([OrderId]) REFERENCES [orders]([id])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [orders]
	ADD  FOREIGN KEY ([UserId]) REFERENCES [users]([id])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [orders]
	ADD  FOREIGN KEY ([shopId]) REFERENCES [shops]([id])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [shop_transactions]
	ADD  FOREIGN KEY ([orderId]) REFERENCES [orders]([id])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [shop_transactions]
	ADD  FOREIGN KEY ([shopId]) REFERENCES [shops]([id])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [shop_transactions]
	ADD  FOREIGN KEY ([id]) REFERENCES [transactions]([id])
		ON DELETE CASCADE
		ON UPDATE CASCADE
go


ALTER TABLE [shops]
	ADD  FOREIGN KEY ([CityId]) REFERENCES [cities]([id])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [transports]
	ADD  FOREIGN KEY ([orderId]) REFERENCES [orders]([id])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [transports]
	ADD  FOREIGN KEY ([cityFromId]) REFERENCES [cities]([id])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [transports]
	ADD  FOREIGN KEY ([cityToId]) REFERENCES [cities]([id])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [user_transactions]
	ADD  FOREIGN KEY ([orderId]) REFERENCES [orders]([id])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [user_transactions]
	ADD  FOREIGN KEY ([userId]) REFERENCES [users]([id])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [user_transactions]
	ADD  FOREIGN KEY ([id]) REFERENCES [transactions]([id])
		ON DELETE CASCADE
		ON UPDATE CASCADE
go


ALTER TABLE [users]
	ADD  FOREIGN KEY ([CityId]) REFERENCES [cities]([id])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


create trigger TR_TRANSFER_MONEY_TO_SHOPS on orders
after update
as begin 

	declare @id int
	declare @oldStatus char(100)
	declare @newStatus char(100)
	declare @date datetime
	declare @shopCursor Cursor
	declare @price decimal(10,3)
	declare @shopId int

	select @id=id, @date=receivetime, @newStatus=status from inserted
	select @oldStatus=status from deleted

	if(@oldStatus = @newStatus)
		return

	update user_transactions set status='paid', finishedAt=@date where orderId=@id;
	update shop_transactions set status='paid', finishedAt=@date where orderId=@id;

	set @shopCursor = cursor for
		select price, shopId from shop_transactions st where orderId=@id

	open @shopCursor

	fetch next from @shopCursor into @price, @shopId

	while @@FETCH_STATUS = 0
	begin
		update shops set credit=credit+@price where id=@shopId
		fetch next from @shopCursor into @price, @shopId
	end

end
go


create PROCEDURE dbo.SP_FINAL_PRICE
	@price decimal(10,3) output,
	@priceWithDiscount decimal(10,3) output,
	@priceWithSystemDicount decimal(10,3) output,
	@orderId int
AS
begin    
	declare @date datetime
	declare @sumOrders decimal(10,3)

	select @priceWithDiscount = sum(o.quantity * (100.0 - cast(s.discount as decimal(10,3))) * cast(a.price as decimal(10,3)) / 100.0),
			@price = sum(o.quantity * a.price)
		from ordered_articles o, articles a, shops s 
		where o.OrderId=@orderId and o.ArticleId=a.id and s.id=a.ShopId

	select @date = date from date

	select @sumOrders = sum(ut.priceWithSystemDiscount) from user_transactions ut, orders o
		where o.id=@orderId and o.UserId=ut.userId and DATEDIFF(day, ut.createdAt, @date) <= 30

	if(@sumOrders > 10000.0)
		set @priceWithSystemDicount = @priceWithDiscount * 98 / 100
	else 
		set @priceWithSystemDicount = @priceWithDiscount

	RETURN @priceWithDiscount
end

go