package student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import operations.ArticleOperations;

public class lm140620_ArticleOperations implements ArticleOperations {

	@Override
	public int createArticle(int shopId, String articleName, int articlePrice) {
		Connection connection = DB.getInstance().getConnection();

		String insertQuery = "insert into articles values(?, ?, ?, ?)";
		try (PreparedStatement ps = connection.prepareStatement(insertQuery,
						PreparedStatement.RETURN_GENERATED_KEYS);) {
			ps.setString(1, articleName);
			ps.setInt(2, articlePrice);
			ps.setInt(3, 0);
			ps.setInt(4, shopId);
			int i = ps.executeUpdate();
			try (ResultSet rs = ps.getGeneratedKeys();) {
				if (rs.next()) {
					return rs.getInt(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return i;
		} catch (Exception e) {
			e.printStackTrace();

		}
		return -1;
	}
}
