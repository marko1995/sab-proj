package student;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import operations.OrderOperations;

public class lm140620_OrderOperations implements OrderOperations {

	Connection connection = DB.getInstance().getConnection();

	@Override
	public int addArticle(int orderId, int articleId, int count) {
		if (count < 0 || !"created".equals(getState(orderId)))
			return -1;
		int quantity = -1;
		String sqlArticle = "select quantity from articles where id=?";
		String sqlUpdate = "update articles set quantity=quantity-? where id=?; "
				+ "if not exists (select * from ordered_articles where orderid=? and articleid=?)"
				+ "insert into ordered_articles values(?, ?, 0);"
				+ "update ordered_articles set quantity=quantity+? where orderid=? and articleid=?;";

		try (PreparedStatement ps = connection.prepareStatement(sqlArticle)) {
			ps.setInt(1, articleId);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					quantity = rs.getInt(1);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (quantity != -1 && quantity >= count) {
			try (PreparedStatement ps = connection.prepareStatement(sqlUpdate)) {
				ps.setInt(1, count);
				ps.setInt(2, articleId);
				ps.setInt(3, orderId);
				ps.setInt(4, articleId);
				ps.setInt(5, articleId);
				ps.setInt(6, orderId);
				ps.setInt(7, count);
				ps.setInt(8, orderId);
				ps.setInt(9, articleId);

				return ps.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return -1;
	}

	@Override
	public int removeArticle(int orderId, int articleId) {
		if (!"created".equals(getState(orderId)))
			return -1;
		String sqlArticle = "select quantity from ordered_articles where orderId=? and articleId=?;";
		String sqlUpdate = "update articles set quantity=quantity+? where id=?;"
				+ "delete from ordered_articles where orderid=? and articleid=?;";
		int quantity = -1;
		try (PreparedStatement ps = connection.prepareStatement(sqlArticle)) {
			ps.setInt(1, orderId);
			ps.setInt(2, articleId);

			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					quantity = rs.getInt(1);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (quantity > 0) {
			try (PreparedStatement ps = connection.prepareStatement(sqlUpdate)) {
				ps.setInt(1, quantity);
				ps.setInt(2, articleId);
				ps.setInt(3, orderId);
				ps.setInt(4, articleId);

				return ps.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return 0;
	}

	@Override
	public List<Integer> getItems(int orderId) {
		String sql = "select articleid from ordered_articles where orderid=?;";
		ArrayList<Integer> list = null;
		try (PreparedStatement ps = connection.prepareStatement(sql)) {
			ps.setInt(1, orderId);
			try (ResultSet rs = ps.executeQuery()) {
				list = new ArrayList<>();
				while (rs.next()) {
					list.add(rs.getInt(1));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public int completeOrder(int orderId) {
		String status = getState(orderId);
		if (!"created".equals(status)) {
			return -1;
		}

		String sqlPrice = "{call SP_FINAL_PRICE (?, ?, ?, ?)}";
		String sqlUser = "select u.id, U.credit, u.cityid from users U, orders O where O.id=? and O.userid=U.id;";

		BigDecimal orderPriceWithDiscount = null;
		BigDecimal orderPriceWithSystemDiscount = null;
		BigDecimal orderPrice = null;
		BigDecimal userCredit = null;
		int userId = -1;
		int userCity = -1;
		try (CallableStatement cs = connection.prepareCall(sqlPrice)) {
			cs.registerOutParameter(1, Types.DECIMAL);
			cs.registerOutParameter(2, Types.DECIMAL);
			cs.registerOutParameter(3, Types.DECIMAL);
			cs.setInt(4, orderId);
			cs.execute();
			orderPrice = new BigDecimal(cs.getInt(1)).setScale(3);
			orderPriceWithDiscount = cs.getBigDecimal(2).setScale(3);
			orderPriceWithSystemDiscount = cs.getBigDecimal(3).setScale(3);
		} catch (SQLException e1) {
			e1.printStackTrace();
			return -1;
		}

		try (PreparedStatement ps = connection.prepareStatement(sqlUser)) {
			ps.setInt(1, orderId);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					userId = rs.getInt(1);
					userCredit = rs.getBigDecimal(2);
					userCity = rs.getInt(3);
				}
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
			return -1;
		}

		if (userCredit == null || orderPrice == null || orderPriceWithSystemDiscount == null
				|| userCredit.compareTo(orderPriceWithSystemDiscount) < 0) {
			return -1;
		}

		String sqlConnection = "select city1id, city2id, distance from connections;";
		List<CityDistance> connections = new ArrayList<>();
		try (Statement s = connection.createStatement()) {
			try (ResultSet rs = s.executeQuery(sqlConnection);) {
				while (rs.next()) {
					connections.add(new CityDistance(rs.getInt(1), rs.getInt(2), rs.getInt(3)));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}

		int j = 0;
		HashMap<Integer, Integer> cityMap = new HashMap<>();
		HashMap<Integer, Integer> cityMapInverse = new HashMap<>();
		List<Distance> connectionsMap = new ArrayList<>();
		for (CityDistance cd : connections) {
			int index1;
			int index2;
			if (cityMap.containsKey(cd.cityId1)) {
				index1 = cityMap.get(cd.cityId1);
			} else {
				index1 = j++;
				cityMap.put(cd.cityId1, index1);
				cityMapInverse.put(index1, cd.cityId1);
			}
			if (cityMap.containsKey(cd.cityId2)) {
				index2 = cityMap.get(cd.cityId2);
			} else {
				index2 = j++;
				cityMap.put(cd.cityId2, index2);
				cityMapInverse.put(index2, cd.cityId2);
			}

			connectionsMap.add(new Distance(index1, index2, cd.distance));
		}

		int indexUser = cityMap.get(userCity);

		int[] d = new int[j];
		int[] t = new int[j];
		calculateDistances(connectionsMap, indexUser, d, t);

		String sqlShops = "select id, cityid from shops;";
		List<Shop> shopList = new ArrayList<>();
		List<ShopIndex> shopIndexList = new ArrayList<>();
		try (Statement s = connection.createStatement()) {
			try (ResultSet rs = s.executeQuery(sqlShops)) {
				while (rs.next()) {
					shopList.add(new Shop(rs.getInt(1), rs.getInt(2)));
					shopIndexList.add(new ShopIndex(rs.getInt(1), cityMap.get(rs.getInt(2))));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}

		int userShopDistance = -1;
		int cityShopIndex = -1;
		int shopIndex = -1;

		for (int i = 0; i < shopIndexList.size(); i++) {
			int shopIndexId = shopIndexList.get(i).indexId;

			int distance = d[shopIndexId];
			if (userShopDistance == -1) {
				userShopDistance = distance;
				cityShopIndex = i;
				shopIndex = shopIndexId;
			} else if (distance != -1 && userShopDistance > distance) {
				userShopDistance = distance;
				cityShopIndex = i;
				shopIndex = shopIndexId;
			}
		}

		if (userShopDistance == -1 || cityShopIndex == -1 || shopIndex == -1)
			return -1;

		Shop toShop = shopList.get(cityShopIndex);

		List<Integer> cityFromShopToUser = new ArrayList<>();
		j = shopIndex;
		cityFromShopToUser.add(cityMapInverse.get(j));
		for (int i = 0; i < d.length; i++) {
			int city = t[j];
			if (city == -1) {
				if (j != indexUser)
					return -1;
				break;
			}
			cityFromShopToUser.add(cityMapInverse.get(city));
			j = city;
		}

		String sqlShopOrders = "select s.cityid " + "from orders o, ordered_articles oa, articles a, shops s "
				+ "where o.id=? and oa.orderid=o.id and a.id=oa.articleid and a.shopid=s.id " + "group by s.cityid;";
		List<Integer> cityIds = new ArrayList<>();
		try (PreparedStatement ps = connection.prepareStatement(sqlShopOrders)) {
			ps.setInt(1, orderId);
			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					cityIds.add(rs.getInt(1));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}

		int maxDistance = 0;

		calculateDistances(connectionsMap, shopIndex, d, t);

		for (Integer i : cityIds) {
			int cityIndex = cityMap.get(i);
			if (maxDistance < d[cityIndex]) {
				maxDistance = d[cityIndex];
			}
		}

		String sqlUpdateOrder = "update orders set status='sent', senttime=(select date from date), "
				+ "shopid=?,days_to_shop=?,days_to_user=? where id=?;" + "update users set credit=? where id=?;";

		try (PreparedStatement ps = connection.prepareStatement(sqlUpdateOrder)) {
			ps.setInt(1, toShop.id);
			ps.setInt(2, maxDistance);
			ps.setInt(3, userShopDistance);
			ps.setInt(4, orderId);
			ps.setBigDecimal(5, userCredit.add(orderPriceWithSystemDiscount.negate()));
			ps.setInt(6, userId);
			int i = ps.executeUpdate();
			if (i == -1)
				return -1;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}

		int transactionId = createTransacton();
		String sqlUserTransaction = "insert into user_transactions values(?,?,?,?,?,'pending',?,?, (select date from date), null)";
		try (PreparedStatement ps = connection.prepareStatement(sqlUserTransaction)) {
			ps.setInt(1, transactionId);
			ps.setBoolean(2, orderPriceWithDiscount.compareTo(orderPriceWithSystemDiscount) != 0);
			ps.setBigDecimal(3, orderPrice);
			ps.setBigDecimal(4, orderPriceWithDiscount);
			ps.setBigDecimal(5, orderPriceWithSystemDiscount);
			ps.setInt(6, orderId);
			ps.setInt(7, userId);

			int i = ps.executeUpdate();
			if (i == -1)
				return -1;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		String sqlShopOrder = "select s.id, "
				+ "sum(cast(a.price as decimal(10,3)) * (100.0 - cast(s.discount as decimal(10,3))) * oa.quantity * 95.0/10000.0),"
				+ "sum(cast(a.price as decimal(10,3)) * (100.0 - cast(s.discount as decimal(10,3))) * oa.quantity /100.0) "
				+ "from articles a, ordered_articles oa, shops s "
				+ "where a.id=oa.articleid and oa.orderid=? and a.shopid=s.id " + "group by s.id;";
		String sqlShopTransaction = "insert into shop_transactions values(?,?,?,?,'pending', (select date from date), null, ?)";
		try (PreparedStatement ps = connection.prepareStatement(sqlShopOrder)) {

			ps.setInt(1, orderId);
			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					int shopId = rs.getInt(1);
					BigDecimal price = rs.getBigDecimal(2).setScale(3);
					BigDecimal allPrice = rs.getBigDecimal(3).setScale(3);
					try (PreparedStatement ps1 = connection.prepareStatement(sqlShopTransaction)) {
						ps1.setInt(1, createTransacton());
						ps1.setInt(2, orderId);
						ps1.setInt(3, shopId);
						ps1.setBigDecimal(4, price);
						ps1.setBigDecimal(5, allPrice);
						int i = ps1.executeUpdate();
						if (i == -1)
							return -1;
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		String transportsSql = "insert into transports values(?,?,?,?,?);";
		int city1Id = cityFromShopToUser.get(0);
		if (cityFromShopToUser.size() > 1) {
			for (int i = 1; i < cityFromShopToUser.size(); i++) {
				city1Id = cityFromShopToUser.get(i - 1);
				int city2Id = cityFromShopToUser.get(i);
				for (CityDistance cd : connections) {
					if (cd.cityId1 == city1Id && cd.cityId2 == city2Id
							|| cd.cityId2 == city1Id && cd.cityId1 == city2Id) {

						try (PreparedStatement ps = connection.prepareStatement(transportsSql)) {
							ps.setInt(1, cd.distance);
							ps.setInt(2, i);
							ps.setInt(3, orderId);
							ps.setInt(4, city1Id);
							ps.setInt(5, city2Id);

							int k = ps.executeUpdate();
							if (k == -1)
								return -1;

						} catch (SQLException e) {
							e.printStackTrace();
						}

					}
				}
			}
		}

		if(maxDistance == 0 && userShopDistance == 0) {
			String sql = "update orders set status='arrived', receivetime=(select date from date) where id=?;";
			try(PreparedStatement ps = connection.prepareStatement(sql)) {
				ps.setInt(1, orderId);
				return ps.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		
		return 1;
	}

	@Override
	public BigDecimal getFinalPrice(int orderId) {
		String sql = "select priceWithSystemDiscount from user_transactions where orderid=?;";

		try (PreparedStatement ps = connection.prepareStatement(sql)) {
			ps.setInt(1, orderId);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					return new BigDecimal(rs.getInt(1)).setScale(3);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public BigDecimal getDiscountSum(int orderId) {
		String sql = "select price - priceWithSystemDiscount from user_transactions where orderid=?;";

		try (PreparedStatement ps = connection.prepareStatement(sql)) {
			ps.setInt(1, orderId);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					return new BigDecimal(rs.getInt(1)).setScale(3);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String getState(int orderId) {
		String query = "select status from orders where id=?;";
		try (PreparedStatement ps = connection.prepareStatement(query);) {
			ps.setInt(1, orderId);
			try (ResultSet rs = ps.executeQuery();) {
				if (rs.next()) {
					return rs.getString(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Calendar getSentTime(int orderId) {
		if (!"created".equals(getState(orderId))) {
			String sql = "select senttime from orders where id=?";
			try (PreparedStatement ps = connection.prepareStatement(sql)) {
				ps.setInt(1, orderId);
				try (ResultSet rs = ps.executeQuery()) {
					if (rs.next()) {
						Date sqlDate = rs.getDate(1);
						Calendar cal = Calendar.getInstance();
						cal.setTime(sqlDate);
						return cal;
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public Calendar getRecievedTime(int orderId) {
		if ("arrived".equals(getState(orderId))) {
			String sql = "select receivetime from orders where id=?";
			try (PreparedStatement ps = connection.prepareStatement(sql)) {
				ps.setInt(1, orderId);
				try (ResultSet rs = ps.executeQuery()) {
					if (rs.next()) {
						Date sqlDate = rs.getDate(1);
						if (sqlDate != null) {
							Calendar cal = Calendar.getInstance();
							cal.setTime(sqlDate);
							return cal;
						}
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public int getBuyer(int orderId) {
		String sql = "select userId from orders where id=?";
		try (PreparedStatement ps = connection.prepareStatement(sql)) {
			ps.setInt(1, orderId);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					return rs.getInt(1);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public int getLocation(int orderId) {
		if ("created".equals(getState(orderId)))
			return -1;

		int daysToUser;
		String sqlOrder = "select days_to_user from orders where id=?;";
		try (PreparedStatement ps = connection.prepareStatement(sqlOrder)) {
			ps.setInt(1, orderId);

			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					daysToUser = rs.getInt(1);
				} else {
					return -1;
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}

		String sqlTransport = "select cityFromId, days, cityToId, count from transports where orderid=? order by count desc;";
		try (PreparedStatement ps = connection.prepareStatement(sqlTransport)) {
			ps.setInt(1, orderId);
			try (ResultSet rs = ps.executeQuery()) {
				if (daysToUser == 0) {
					if (rs.next()) {
						return rs.getInt(3);
					}
				} else {
					int distanceLeft = 0;
					while (rs.next()) {
						int cityId = rs.getInt(1);
						distanceLeft += rs.getInt(2);
						if (distanceLeft >= daysToUser) {
							return cityId;
						}
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return 0;
	}

	private int createTransacton() {
		String sql = "insert into transactions default values";
		try (PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			int i = ps.executeUpdate();
			if (i != -1) {
				try (ResultSet rs = ps.getGeneratedKeys()) {
					if (rs.next()) {
						return (int) rs.getLong(1);
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	private static void calculateDistances(List<Distance> distances, int index, int[] d, int[] t) {
		if (d == null || t == null || distances == null || distances.isEmpty() || d.length != t.length)
			return;

		for (int i = 0; i < d.length; i++) {
			d[i] = -1;
			t[i] = -1;
		}

		d[index] = 0;

		List<Integer> s = new ArrayList<>();
		s.add(index);

		List<Distance> city = new ArrayList<>();
		for (Distance distance : distances) {
			if (distance.index1 == index) {
				city.add(distance);
			} else if (distance.index2 == index) {
				Distance tDistance = new Distance(distance.index2, distance.index1, distance.distance);
				city.add(tDistance);
			}
		}

		for (Distance dd : city) {
			d[dd.index2] = dd.distance;
			t[dd.index2] = index;
		}

		for (int i = 0; i < d.length; i++) {

			int offset = -1;
			int currentIndex = -1;

			for (int j = 0; j < d.length; j++) {
				if ((d[j] != -1) && (offset == -1 || offset > d[j]) && !s.contains(j)) {
					offset = d[j];
					currentIndex = j;
				}
			}

			if (currentIndex == -1)
				break;

			s.add(currentIndex);

			city = new ArrayList<>();
			for (Distance distance : distances) {
				if (distance.index1 == currentIndex) {
					if (!s.contains(distance.index2)) {
						city.add(distance);
					}
				} else if (distance.index2 == currentIndex) {
					if (!s.contains(distance.index1)) {
						Distance tDistance = new Distance(distance.index2, distance.index1, distance.distance);
						city.add(tDistance);
					}
				}
			}

			for (Distance dd : city) {
				if (d[dd.index2] == -1 || (d[dd.index2] > offset + dd.distance)) {
					d[dd.index2] = offset + dd.distance;
					t[dd.index2] = dd.index1;
				}
			}
		}
	}

	private static class Distance {
		int index1;
		int index2;
		int distance;

		protected Distance(int index1, int index2, int distance) {
			this.index1 = index1;
			this.index2 = index2;
			this.distance = distance;
		}
	}

	private static class CityDistance {
		int cityId1;
		int cityId2;
		int distance;

		protected CityDistance(int cityId1, int cityId2, int distance) {
			this.cityId1 = cityId1;
			this.cityId2 = cityId2;
			this.distance = distance;
		}
	}

	private static class Shop {
		int id;
		int cityid;

		protected Shop(int id, int cityid) {
			this.id = id;
			this.cityid = cityid;
		}
	}

	private static class ShopIndex {
		int id;
		int indexId;

		protected ShopIndex(int id, int indexid) {
			this.id = id;
			this.indexId = indexid;
		}
	}

	public static void main(String[] arg) {

		List<Distance> list = new ArrayList<>();

		list.add(new Distance(0, 1, 5));
		list.add(new Distance(0, 2, 8));
		list.add(new Distance(0, 3, 2));
		list.add(new Distance(2, 3, 2));
		list.add(new Distance(1, 3, 9));
		list.add(new Distance(1, 4, 3));
		list.add(new Distance(3, 4, 7));
		list.add(new Distance(3, 8, 8));
		list.add(new Distance(4, 5, 3));
		list.add(new Distance(4, 6, 7));
		list.add(new Distance(5, 6, 4));
		list.add(new Distance(7, 8, 5));
		list.add(new Distance(5, 7, 5));
		list.add(new Distance(6, 7, 6));

		int[] d = new int[9];
		int[] t = new int[9];

		int index = 3;

		calculateDistances(list, index, d, t);

		System.out.print("");
	}
}
