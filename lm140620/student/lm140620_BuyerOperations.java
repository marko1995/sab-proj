package student;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import operations.BuyerOperations;

public class lm140620_BuyerOperations implements BuyerOperations {

	Connection connection = DB.getInstance().getConnection();

	@Override
	public int createBuyer(String name, int cityId) {
		String insertQuery = "insert into users values(?,?,0)";
		try (PreparedStatement ps = connection.prepareStatement(insertQuery,
						PreparedStatement.RETURN_GENERATED_KEYS);) {
			ps.setString(1, name);
			ps.setInt(2, cityId);

			int i = ps.executeUpdate();
			try (ResultSet rs = ps.getGeneratedKeys();) {
				if (rs.next()) {
					return rs.getInt(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return i;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public int setCity(int buyerId, int cityId) {
		String insertQuery = "update users set cityid=? where id=?";
		try (PreparedStatement ps = connection.prepareStatement(insertQuery,
						PreparedStatement.RETURN_GENERATED_KEYS);) {
			ps.setInt(1, cityId);
			ps.setInt(2, buyerId);
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public int getCity(int buyerId) {
		String query = "select CityId from users where id=?";
		try (PreparedStatement ps = connection.prepareStatement(query);) {
			ps.setInt(1, buyerId);
			try (ResultSet rs = ps.executeQuery();) {
				if (rs.next()) {
					return rs.getInt(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return -1;
	}

	@Override
	public BigDecimal increaseCredit(int buyerId, BigDecimal credit) {
		String insertQuery = "update users set credit=credit+? where id=?";
		try (PreparedStatement ps = connection.prepareStatement(insertQuery,
						PreparedStatement.RETURN_GENERATED_KEYS);) {
			ps.setBigDecimal(1, credit);
			ps.setInt(2, buyerId);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return getCredit(buyerId);
	}

	@Override
	public int createOrder(int buyerId) {
		String insertQuery = "insert into orders values(?,?, null, null, null, null, null);";
		try (PreparedStatement ps = connection.prepareStatement(insertQuery,
						PreparedStatement.RETURN_GENERATED_KEYS);) {
			ps.setString(1, "created");
			ps.setInt(2, buyerId);
			int i = ps.executeUpdate();
			try (ResultSet rs = ps.getGeneratedKeys();) {
				if (rs.next()) {
					return rs.getInt(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return i;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public List<Integer> getOrders(int buyerId) {
		String query = "select id from orders where userid=?;";
		List<Integer> list = null;
		try (PreparedStatement ps = connection.prepareStatement(query)) {
			ps.setInt(1, buyerId);
			try(ResultSet rs = ps.executeQuery();){
				list = new ArrayList<>();
				while(rs.next()) {
					list.add(rs.getInt(1));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public BigDecimal getCredit(int buyerId) {
		String query = "select credit from users where id=?";
		try (PreparedStatement ps = connection.prepareStatement(query);) {
			ps.setInt(1, buyerId);
			try (ResultSet rs = ps.executeQuery();) {
				if (rs.next()) {
					return rs.getBigDecimal(1).setScale(3);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
