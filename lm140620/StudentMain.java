import operations.*;
import student.*;

import org.junit.Test;
import tests.TestHandler;
import tests.TestRunner;

import java.util.Calendar;
import java.util.List;

public class StudentMain {

    public static void main(String[] args) {

        ArticleOperations articleOperations = new lm140620_ArticleOperations(); // Change this for your implementation (points will be negative if interfaces are not implemented).
        BuyerOperations buyerOperations = new lm140620_BuyerOperations();
        CityOperations cityOperations = new lm140620_CityOperations();
        GeneralOperations generalOperations = new lm140620_GeneralOperations();
        OrderOperations orderOperations = new lm140620_OrderOperations();
        ShopOperations shopOperations = new lm140620_ShopOperations();
        TransactionOperations transactionOperations = new lm140620_TransactionOperations();
//
//        Calendar c = Calendar.getInstance();
//        c.clear();
//        c.set(2010, Calendar.JANUARY, 01);
//
//
//        Calendar c2 = Calendar.getInstance();
//        c2.clear();
//        c2.set(2010, Calendar.JANUARY, 01);
//
        
        TestHandler.createInstance(
                articleOperations,
                buyerOperations,
                cityOperations,
                generalOperations,
                orderOperations,
                shopOperations,
                transactionOperations
        );

        TestRunner.runTests();
    }
}
