create PROCEDURE dbo.SP_FINAL_PRICE
	@price decimal(10,3) output,
	@priceWithDiscount decimal(10,3) output,
	@priceWithSystemDicount decimal(10,3) output,
	@orderId int
AS
begin    
	declare @date datetime
	declare @sumOrders decimal(10,3)

	select @priceWithDiscount = sum(o.quantity * (100.0 - cast(s.discount as decimal(10,3))) * cast(a.price as decimal(10,3)) / 100.0),
			@price = sum(o.quantity * a.price)
		from ordered_articles o, articles a, shops s 
		where o.OrderId=@orderId and o.ArticleId=a.id and s.id=a.ShopId

	select @date = date from date

	select @sumOrders = sum(ut.priceWithSystemDiscount) from user_transactions ut, orders o
		where o.id=@orderId and o.UserId=ut.userId and DATEDIFF(day, ut.createdAt, @date) <= 30

	if(@sumOrders > 10000.0)
		set @priceWithSystemDicount = @priceWithDiscount * 98 / 100
	else 
		set @priceWithSystemDicount = @priceWithDiscount

	RETURN @priceWithDiscount
end