create trigger TR_TRANSFER_MONEY_TO_SHOPS on orders
after update
as begin 

	declare @id int
	declare @oldStatus char(100)
	declare @newStatus char(100)
	declare @date datetime
	declare @shopCursor Cursor
	declare @price decimal(10,3)
	declare @shopId int

	select @id=id, @date=receivetime, @newStatus=status from inserted
	select @oldStatus=status from deleted

	if(@oldStatus = @newStatus)
		return

	update user_transactions set status='paid', finishedAt=@date where orderId=@id;
	update shop_transactions set status='paid', finishedAt=@date where orderId=@id;

	set @shopCursor = cursor for
		select price, shopId from shop_transactions st where orderId=@id

	open @shopCursor

	fetch next from @shopCursor into @price, @shopId

	while @@FETCH_STATUS = 0
	begin
		update shops set credit=credit+@price where id=@shopId
		fetch next from @shopCursor into @price, @shopId
	end

end

